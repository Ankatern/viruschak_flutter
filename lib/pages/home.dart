import 'dart:math';

import 'package:flutter/material.dart';
import 'package:virushack/services/care.dart';
import 'package:virushack/services/careList.dart';
import 'package:virushack/services/pain.dart';
import 'package:virushack/services/patient.dart';
import 'package:virushack/services/reception.dart';
import 'package:virushack/services/user.dart';
import 'package:virushack/widgets/careCalendar.dart';
import 'package:virushack/widgets/careListConntainer.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Pain> pains = [
    new Pain(1, 'Pain', 'Description', datetime: '1974-02-20 00:00:00'),
    new Pain(2, 'Pain 2', 'Description 2', datetime: '1974-02-20 00:00:00'),
    new Pain(3, 'Pain 3', 'Description 3', datetime: '1974-02-20 00:00:00')
  ];

  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _appBars = [
      Text('Календарь заданий'),
      Text('Список заданий на уход'),
//      Text('Дневник боли')
    ];
    List<Widget> _widgetOptions = [
      CareCalendar(events: createRandomCareListsList(),),
      CareListContainer(careList: getEventList()),
//      PainList(pains: pains)
    ];

    return Scaffold(
      appBar: AppBar(
        title: _appBars.elementAt(_selectedIndex),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.description),
              title: Text('Календарь')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.description),
            title: Text('Список уходов')
          ),
//          BottomNavigationBarItem(
//            icon: Icon(Icons.ac_unit),
//            title: Text('Дневник боли')
//          )
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue[400],
        onTap: _onItemTapped,
      ),
    );
  }


  Map<DateTime, List> createRandomCareListsList() {
    Map<DateTime, List> events = {};
    final now = DateTime.now();
    DateTime selectedDay = new DateTime(now.year, now.month, now.day);
    List todayEvents = [];
    for (var i = 0; i < Random().nextInt(5) + 1; i++) {
      todayEvents.add(createRandomCareList(Random().nextInt(5)));
    }
    events[selectedDay] = todayEvents;

    //previous
    for (var i = 0; i < Random().nextInt(3) + 1; i++) {
      int day = Random().nextInt(10) + 1;
      List eventsList = [];
      for (var i = 0; i < Random().nextInt(7) + 1; i++) {
        eventsList.add(createRandomCareList(Random().nextInt(5)));
      }
      events[selectedDay.subtract(Duration(days: day))] = eventsList;
    }

    //future
    for (var i = 0; i < Random().nextInt(10) + 1; i++) {
      int day = Random().nextInt(31);
      List eventsList = [];
      for (var i = 0; i < Random().nextInt(7) + 1; i++) {
        eventsList.add(createRandomCareList(Random().nextInt(5)));
      }
      events[selectedDay.add(Duration(days: day))] = eventsList;
    }

    return events;
  }
  List getEventList() {
    List eventsList = [];
    for (var i = 0; i < Random().nextInt(7) + 1; i++) {
      eventsList.add(createRandomCareList(Random().nextInt(5)));
    }
    return eventsList;
  }

  CareList createRandomCareList(int careCount) {
    List<Care> cares = [];
    for (var i = 0; i < careCount + 1; i++) {
      cares.add(createRandomCare());
    }
    Patient patient = Patient(
        Random().nextInt(300),
        'Имя клиента ' + Random().nextInt(100).toString(),
        '8-999-333-' + (Random().nextInt(89) + 10).toString() + (Random().nextInt(89) + 10).toString(),
        getRandomAddress()
    );
    User user = User(
        Random().nextInt(999),
        getRandomAddress(),
        '8-999-333-' + (Random().nextInt(89) + 10).toString() + (Random().nextInt(89) + 10).toString()
    );
    String description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
    return CareList(Random().nextInt(300), 'Уход  ' + Random().nextInt(300).toString(), description, cares, '2020-02-20 00:00:00', true, patient, user);
  }

  static Care createRandomCare() {
    return Care(
        Random().nextInt(300),
        'Уход  ' + Random().nextInt(300).toString(),
        'At vero eos et accusamus et iusto odio /*dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores',
        datetime: '2020-02-20 00:00:00',
        priority: Random().nextInt(10),
        category: 'Категория по умолчанию',
        reception: getRandomReception(),
        periodicity: getRandomPeriodicity()
    );
  }

  String getRandomAddress() {
    switch(Random().nextInt(5)) {
      case 0:
        return 'ул. Русская д30 кв45';
      case 1:
        return 'ул. Фонвизинская д26 кв11';
      case 2:
        return 'ул. Ходовская д13 кв13';
      case 3:
        return 'блв. Дмитрия Донского кв129';
      case 4:
        return 'ул Куйбышева д17 кв 26';
      case 5:
        return 'блв. Хруничева кв28';
    }
  }

  static getRandomReception() {
    return Reception(Random().nextInt(900), 'At vero eos et accusamus et iusto odio /*dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.*/');
  }

  static getRandomPeriodicity() {
    switch(Random().nextInt(3)) {
      case 0:
        return 'Раз в день';
      case 1:
        return 'Два раза в день';
      case 2:
        return 'Раз в неделю';
      case 3:
        return 'Раз в месяц';
    }
  }
}
