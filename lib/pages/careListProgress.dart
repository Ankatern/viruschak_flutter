import 'package:flutter/material.dart';
import 'package:virushack/services/care.dart';
import 'package:virushack/services/careList.dart';
import 'package:virushack/widgets/careContainer.dart';

class CareListProgress extends StatefulWidget {

  @override
  _CareListProgressState createState() => _CareListProgressState();
}

class _CareListProgressState extends State<CareListProgress> {
  Map data = {};

  @override
  Widget build(BuildContext context) {
    data = ModalRoute.of(context).settings.arguments;
    CareList careList = data['careList'];
    return Scaffold(
      appBar: AppBar(
        title: Text(careList.title),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20.0
        ),
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top:20.0, bottom: 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:<Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Text('Описание задания',
                      style: TextStyle(
                        fontSize: 20.0
                      ),
                    ),
                  ),
                  Text(careList.description)
                ],
              )
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(bottom: 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:<Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: Text('Информация о пациенте',
                      style: TextStyle(
                        fontSize: 20.0
                      ),
                    ),
                  ),
                  Text('Имя: ' + careList.patient.name),
                  Text('Дата ухода: ' + careList.startTime.toString()),
                  Text('Телефон пациента: ' + careList.patient.phone),
                  Text('Телефон близкого: ' + careList.user.phone),
                  Text('Адрес: ' + careList.patient.address),
                ],
              )
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:<Widget>[
                Text('Процедуры', style: TextStyle(fontSize: 20.0)),
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: careList.cares.length,
                  itemBuilder: (context, index) {
                    Care care = careList.cares[index];
                    return CareContainer(index: index, care: care, checkbox: true);
                  },
                )
              ],
            ),
            ButtonTheme(
              minWidth: MediaQuery.of(context).size.width,
              child: RaisedButton(
                child: Text('Завершить процедуры'),
                onPressed: (){
                  Navigator.pushNamed(context, '/care_list_finish', arguments: {'careList': careList});
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
