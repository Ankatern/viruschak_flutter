import 'package:flutter/material.dart';

class CareListFinish extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.25,
          left: 20.0,
          right: 20.0
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.beenhere,
              size: 100.0,
            ),
            SizedBox(
              height: 40.0,
            ),
            Text('Следующий уход за пациентом состоится 11.11.20 в 9:30'),
            SizedBox(
              height: 10.0,
            ),
            Text('Продолжайте в том же духе!'),
            SizedBox(
              height: 40.0,
            ),
            ButtonTheme(
              minWidth: MediaQuery.of(context).size.width - 60.0,
              child: RaisedButton(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Text('Завершить процедуры'),
                onPressed: (){
                  Navigator.pushNamed(context, '/');
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
