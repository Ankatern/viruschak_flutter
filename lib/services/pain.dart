import 'base.dart';

class Pain extends Base {
  Pain(int id, String title, String description, {String datetime}) :
        super(id, title, description, datetime: datetime);
}