import 'package:intl/intl.dart';

class Base {
  String title;
  String description;
  DateTime datetime;
  String formattedDateTime;
  int _id;

  Base(int this._id, String this.title, String this.description, {String datetime} ) {
    var formatter = new DateFormat('yyyy-MM-dd H:m');
    if (datetime != null) {
      this.datetime = DateTime.parse(datetime);
      this.formattedDateTime = formatter.format(this.datetime);
    }
  }

//  factory Base.fromJson(dynamic json) {
//    return Base(
//        title: json['title'] as String,
//        description: json['description'] as String,
//        datetime: json['datetime'] as String
//    );
//  }
}