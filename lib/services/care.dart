import 'package:virushack/services/reception.dart';

import 'base.dart';

class Care extends Base {
  Reception reception;

  int priority;
  String category;
  String periodicity;

  bool checked = false;

  Care(int id, String title, String description, {String datetime, int this.priority, this.category, this.reception, this.periodicity}) :
        super(id, title, description, datetime: datetime){
    this.checked = false;
  }
}