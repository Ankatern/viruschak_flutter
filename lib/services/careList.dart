import 'package:intl/intl.dart';
import 'package:virushack/services/care.dart';
import 'package:virushack/services/patient.dart';
import 'package:virushack/services/user.dart';

class CareList {
  int _id;
  String title;
  String description;
  List<Care> cares;
  DateTime startTime;
  DateTime endTime;
  bool state;
  DateTime closedAt;

  String formattedStartTime;
  String formattedEndTime;
  String formattedClosedAt;

  Patient patient;
  User user;

  CareList(
      this._id,
      this.title,
      this.description,
      this.cares,
      String startTime,
      this.state,
      this.patient,
      this.user,
      {String endTime, String closedAt}
  ) {
    var formatter = new DateFormat('yyyy-MM-dd Hm');
    if (startTime != null) {
      this.startTime = DateTime.parse(startTime);
      this.formattedStartTime = formatter.format(this.startTime);
    }

    if (endTime != null) {
      this.endTime = DateTime.parse(endTime);
      this.formattedEndTime = formatter.format(this.endTime);
    }

    if (closedAt != null) {
      this.closedAt = DateTime.parse(closedAt);
      this.formattedClosedAt = formatter.format(this.closedAt);
    }
  }
}