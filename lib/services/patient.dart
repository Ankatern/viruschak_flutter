class Patient {
  int _id;
  String name;
  String phone;
  String address;

  Patient(this._id, this.name, this.phone, this.address);
}