import 'package:flutter/material.dart';
import 'package:virushack/services/care.dart';
import 'package:virushack/services/careList.dart';

import 'baseTitle.dart';

class CaresList extends StatefulWidget {
  CareList careList;

  CaresList({this.careList});

  @override
  _CaresListState createState() => _CaresListState();
}

class _CaresListState extends State<CaresList> {
  @override
  Widget build(BuildContext context) {
    var cares = widget.careList.cares;
    return ListView.builder(
      itemCount: cares.length,
      itemBuilder: (context, index) {
        Care care = cares[index];
        return Container(
          decoration: BoxDecoration(
            border: Border.all(width: 0.8),
          ),
          margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              BaseTitle(child: care),
              Spacer(),
              IconButton(
                icon: Icon(Icons.list),
                onPressed: () {
                  print('show');
                },
              ),
              IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  print('delete');
                },
              )
            ],
          ),
        );
      },
    );
  }
}
