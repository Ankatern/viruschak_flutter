import 'package:flutter/material.dart';
import 'package:virushack/services/base.dart';

class BaseTitle extends StatefulWidget {
  final Base child;

  BaseTitle({this.child});
  @override
  _BaseTitleState createState() => _BaseTitleState();
}

class _BaseTitleState extends State<BaseTitle> {
  @override
  Widget build(BuildContext context) {
    if (widget.child.formattedDateTime == null) {
      return Text(widget.child.title);
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(widget.child.title),
          Text(widget.child.formattedDateTime.toString()),
        ],
      );
    }
  }
}
