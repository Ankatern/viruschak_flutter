import 'package:flutter/material.dart';
import 'package:virushack/pages/careListInfo.dart';
import 'package:virushack/services/careList.dart';

class CareListContainer extends StatefulWidget {
  List careList;

  CareListContainer({this.careList});

  @override
  _CareListContainerState createState() => _CareListContainerState();
}

class _CareListContainerState extends State<CareListContainer> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.careList.length,
      itemBuilder: (context, index) {
        CareList careList = widget.careList[index];
        return InkWell(
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(width: 0.8),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            padding: const EdgeInsets.all(10.0),
            child:  Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  careList.title,
                  style: TextStyle(
                    fontSize: 20.0
                  ),
                ),
                Row(children: <Widget>[
                  Text('Имя пациента: '),
                  Text(careList.patient.name)
                ]),
                Row(children: <Widget>[
                  Text('Дата посещения: '),
                  Text(careList.formattedStartTime.toString())
                ]),
                Row(children: <Widget>[
                  Text('Адрес: '),
                  Text(careList.patient.address)
                ]),
              ],
            )
          ),
          onTap: () {
            Navigator.pushNamed(context, '/care_list_info', arguments: {'careList': careList});
          },
        );
      },
    );
  }
}
