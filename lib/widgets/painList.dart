import 'package:flutter/material.dart';
import 'package:virushack/services/pain.dart';
import 'package:virushack/widgets/baseTitle.dart';

class PainList extends StatefulWidget {
  List<Pain> pains;

  PainList({this.pains});

  @override
  _PainListState createState() => _PainListState();
}

class _PainListState extends State<PainList> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.pains.length,
      itemBuilder: (context, index) {
        Pain pain = widget.pains[index];
        return Container(
          padding: const EdgeInsets.all(10.0),
          color: Colors.amberAccent,
          child: Row(
            children: <Widget>[
              BaseTitle(child: pain),
              Spacer(),
              IconButton(
                icon: Icon(Icons.list),
                onPressed: () {
                  print('show');
                },
              ),
              IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  print('delete');
                },
              )
            ],
          ),
        );
      },
    );
  }
}
