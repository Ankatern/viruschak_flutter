import 'package:animated_size_and_fade/animated_size_and_fade.dart';
import 'package:flutter/material.dart';
import 'package:virushack/services/care.dart';

class CareContainer extends StatefulWidget {
  final int index;
  final Care care;
  bool checkbox = false;


  CareContainer({this.index, this.care, this.checkbox = false});

  @override
  _CareContainerState createState() => _CareContainerState();
}

class _CareContainerState extends State<CareContainer> with TickerProviderStateMixin {
  bool toggle;

  @override
  void initState() {
    super.initState();
    toggle = false;
  }

  @override
  Widget build(BuildContext context) {
    var widgetShow = Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Категория: '),
            Text(widget.care.category)
          ],
        ),
        Row(
          children: <Widget>[
            Text('Приоритет: '),
            Text(widget.care.priority.toString())
          ],
        ),
        Row(
          children: <Widget>[
            Text('Периодичность: '),
            Text(widget.care.periodicity)
          ],
        ),
        Divider(color: Colors.black),
        Text(widget.care.description),
        Container(
          padding: EdgeInsets.all(10.0),
          child: Text(widget.care.reception.description),
          decoration: BoxDecoration(
            border: Border.all(width: 0.8),
          ),
        ),
        Divider(color: Colors.black),
        TextFormField(
          decoration: const InputDecoration(
            hintText: 'Поле для ввода комментария',
          )
        ),
        ButtonTheme(
          minWidth: MediaQuery.of(context).size.width,
          child: RaisedButton(
            child: Text('Загрузить результат'),
            onPressed: (){},
          ),
        )
      ],
    );
    var widgetCheckbox = Checkbox(
      value: widget.care.checked,
      onChanged: (bool value){
        setState(() {
          widget.care.checked = value;
        });
      },
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children:<Widget>[
            Text(
                'Процедура ' + (widget.index + 1).toString(),
                style: TextStyle(fontSize: 20.0)
            ),
            Spacer(),
            widget.checkbox ? widgetCheckbox : Spacer(),
            IconButton(
              icon: Icon(toggle ? Icons.arrow_upward : Icons.arrow_downward),
              onPressed: () {
                setState(() {
                  toggle = !toggle;
                });
              },
            )
          ]
        ),
        AnimatedSizeAndFade(
          vsync: this,
          child: toggle ? widgetShow : Container(),
          fadeDuration: const Duration(milliseconds: 300),
          sizeDuration: const Duration(milliseconds: 600),
        )
      ],
    );
  }
}
