import 'package:flutter/material.dart';
import 'package:virushack/pages/careListFinish.dart';
import 'package:virushack/pages/careListInfo.dart';
import 'package:virushack/pages/careListProgress.dart';
import 'package:virushack/pages/home.dart';

void main() => runApp(MaterialApp(
  initialRoute: '/',
  routes: {
    '/': (context) => Home(),
    '/care_list_info': (context) => CareListInfo(),
    '/care_list_progress': (context) => CareListProgress(),
    '/care_list_finish': (context) => CareListFinish(),
  },
  theme: ThemeData(
    primaryColor: Colors.lightBlue[800],
  ),
));